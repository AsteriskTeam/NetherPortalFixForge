package kengxxiao.netherportalfix.coremod;

import java.util.Arrays;

import net.minecraftforge.fml.common.DummyModContainer;
import net.minecraftforge.fml.common.LoadController;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.eventhandler.EventBus;

public class NetherPortalFixForgeModContainer extends DummyModContainer{
	public NetherPortalFixForgeModContainer()
	{
		super(new ModMetadata());
		ModMetadata meta = getMetadata();
		meta.modId = "netherportalfixforge";
		meta.name = "NetherPortalFixForge";
		meta.version = "1.0.0";
		meta.authorList = Arrays.asList("Kengxxiao");
		meta.description = "The forge version of NetherPortalFix.";
		this.setEnabledState(true);
	}
	public boolean registerBus(EventBus bus, LoadController controller)
	{
		bus.register(this);
		return true;
	}
}
