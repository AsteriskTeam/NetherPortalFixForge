package kengxxiao.netherportalfix.coremod;

import java.util.ListIterator;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.MethodNode;

import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraftforge.fml.common.asm.transformers.deobf.FMLDeobfuscatingRemapper;

public class NetherPortalFixForgeClassTransformer implements IClassTransformer{

	private FMLDeobfuscatingRemapper deobf = FMLDeobfuscatingRemapper.INSTANCE;
	
	@Override
	public byte[] transform(String name, String transformedName, byte[] basicClass) {
		if (transformedName.equals("net.minecraft.client.entity.EntityPlayerSP"))
		{
			ClassNode classNode = new ClassNode();
            ClassReader classReader = new ClassReader(basicClass);
            classReader.accept(classNode, 0);
            
            MethodNode m = findMethod(classNode, name, "()V", "func_70636_d");
            ListIterator<AbstractInsnNode> iterator = m.instructions.iterator();
            int hit = 0;
            while (iterator.hasNext())
            {
            	 AbstractInsnNode node = iterator.next();
            	 if (node instanceof FieldInsnNode)
            	 {
            		 FieldInsnNode fieldNode = (FieldInsnNode) node;
            		 if (fieldNode.getNext().getOpcode() == Opcodes.GETFIELD && hit++ == 1)
            		 {
            			 JumpInsnNode jmpNode = (JumpInsnNode) fieldNode.getNext().getNext().getNext(); 
            			 m.instructions.set(jmpNode, new JumpInsnNode(Opcodes.IFEQ, jmpNode.label));
            		 }
            	 }
            }
            
            ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS | ClassWriter.COMPUTE_FRAMES);
            classNode.accept(classWriter);
            return classWriter.toByteArray();
		}
		return basicClass;
	}

	private MethodNode findMethod(ClassNode classNode,String obfedClassName , String desc, String... names) {
		for (MethodNode method : classNode.methods) {
			if (desc != null && !desc.equals(deobf.mapMethodDesc(method.desc)))
				continue;

			for (String name : names) {
				if (deobf.mapMethodName(obfedClassName, method.name, method.desc).equals(name))
					return method;
			}
		}
		return null;
	}

}
